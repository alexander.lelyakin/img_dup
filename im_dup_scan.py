# /usr/bin/python3
import sys

import fp_db_sqlite

from util import proceed_args

EXT = ['.jpg', '.jpeg']

fp_db_sqlite.db_open()


def action(p):
    fp_db_sqlite.FileInfo(p, from_db=True, from_fs=True)


if __name__ == "__main__":
    proceed_args(args=sys.argv[1:], action=action, extensions=EXT)
