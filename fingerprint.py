import gmpy2
from PIL import Image, ImageOps, ImageFilter

from util import FilePath, FP_input


def img_fingerprint(img_name: FP_input) -> bytes:
    """
    Calculate a fingerprint of image file
    :param img_name: filename
    :return: fingerprint as 32 bytes string
    """
    try:
        img_name = FilePath(img_name)
        if not img_name.exists():
            return b''
        im: Image.Image = Image.open(img_name.path)
        im = im.resize((160, 160), Image.BILINEAR)
        im = ImageOps.grayscale(im)
        im = im.filter(ImageFilter.GaussianBlur(2))
        im = ImageOps.autocontrast(im)
        im = ImageOps.equalize(im)
        im = im.resize((16, 16), Image.BOX)
        im = im.point(lambda x: x > 127, '1')
        return im.tobytes()
    except IOError:
        return b''


def fp_rot90(fp: bytes) -> bytes:
    """
    Rotate image fingerprint 90 deg
    :param fp: fingerprint
    :return: rotated fingerprint
    """
    im = Image.frombytes('1', (16, 16), fp)
    im = im.transpose(Image.ROTATE_90)
    return im.tobytes()


def fp_show(fp: bytes) -> None:
    """
    Show fingerprint image on screen
    :param fp: fingerprint
    :return:
    """
    im = Image.frombytes('1', (16, 16), fp)
    im.resize((160, 160)).show()


def fp_diff(fp1: bytes, fp2: bytes) -> float:
    """
    Estimate difference between fingerprints in percents
    :param fp1: one fingerprint
    :param fp2: another fingerprint
    :return: difference: 100% - same; 0% - inverted, 50% unrelated
    """
    return (1.0 - gmpy2.popcount(int.from_bytes(fp1, 'little') ^ int.from_bytes(fp2, 'little')) / 256.0) * 100.0


def fp_hex(fp: bytes) -> str:
    """
    Convert fingerprint to printable hex string
    :param fp: fingerprint
    :return: printable representation of fp
    """
    return ''.join('%02x' % b for b in fp)
