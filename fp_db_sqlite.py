#!/usr/bin/python3

import re
import sqlite3
import sys
from hashlib import md5
from typing import Optional, Tuple, Callable, Generator, Union, Pattern, List, Sequence

from fingerprint import img_fingerprint, fp_rot90, fp_diff
from util import FilePath, FP_input

db_t = sqlite3.Connection
DB: Optional[sqlite3.Connection] = None


class FileInfo:
    file: FilePath
    fingerprint: bytes
    size: int
    mtime: int
    inode: int
    md5sum: bytes

    def __init__(self, file_name: FP_input, *,
                 from_db: bool = False, from_fs: bool = False, from_row: Sequence = None,
                 fp: bytes = b''):
        self.file = FilePath(file_name)
        self.zero()
        if from_db or from_row:
            if from_db is True:
                self.get_db()
            elif from_row:
                self.from_row(from_row)

            if from_fs and not self.check():
                self.get_fs(fp=fp)
                self.put_db()
        elif from_fs:
            self.get_fs(fp=fp)

    def zero(self):
        self.size = 0
        self.mtime = 0
        self.inode = 0
        self.md5sum = b''
        self.fingerprint = b''

    def stat(self):
        if not self.file.exists():
            self.zero()
            return
        stat = self.file.path.stat()
        self.size = stat.st_size
        self.mtime = int(stat.st_mtime)
        self.inode = stat.st_ino

    def check(self) -> bool:
        if not self.file.exists():
            return False
        if not self.fingerprint:
            return False
        stat = self.file.path.stat()
        return (stat.st_size == self.size) and \
               (int(stat.st_mtime) == self.mtime) and \
               (stat.st_ino == self.inode)

    def get_fs(self, fp: Optional[bytes] = None):
        self.stat()
        if not self.size:
            return

        self.md5sum = self.get_digest()

        if not fp:
            fp = img_fingerprint(self.file)
        self.fingerprint = fp

    def chunks(self) -> Generator[bytes, None, None]:
        f = self.file.path.open('rb')
        while True:
            buf: bytes = f.read(4096)
            if len(buf) == 0:
                return
            yield buf

    def get_digest(self):
        d = md5()
        for chunk in self.chunks():
            d.update(chunk)
        return d.digest()

    def put_db(self):
        if self.size and self.fingerprint:
            DB.execute("REPLACE INTO images(filename,fingerprint,`size`,inode,mtime,md5sum) VALUES (?,?,?,?,?,?)",
                       (self.file.str,
                        self.fingerprint,
                        self.size,
                        self.inode,
                        self.mtime,
                        self.md5sum)
                       )
        else:
            DB.execute("DELETE FROM images WHERE filename=?", (self.file.str,))
        DB.commit()

    def from_row(self, row):
        if row:
            self.fingerprint, self.size, self.inode, self.mtime, self.md5sum = row
        else:
            self.zero()

    def get_db(self):
        cur = DB.execute("SELECT * from images WHERE filename=?", (self.file.str,))
        row = cur.fetchone()
        if row is None:
            self.zero()
        else:
            self.from_row(row[1:])


def db_open(name: FP_input = '~/images.sqlite') -> db_t:
    """
    Open database
    :param name: database filename
    :return: the database
    """
    global DB
    if not DB:
        name = FilePath(name)
        DB = sqlite3.connect(name.str)
    return DB


def db_fingerprint(img_name: FP_input) -> bytes:
    """
    Get the fingerprint of file from database
    :param img_name: filename
    :return: fingerprint
    """
    db_open()
    fi = FileInfo(img_name, from_db=True)
    return fi.fingerprint


def db_update(iname: FP_input, fp: bytes = b'') -> bytes:
    """
    Update fingerprint in the database
    :param iname: filename
    :param fp: fingerprint. If not specified will be calculated
    :return: fingerprint
    """
    db_open()
    f_i = FileInfo(iname, from_db=False, from_fs=True, fp=fp)
    f_i.put_db()
    return f_i.fingerprint


def fingerprint(img_path: FP_input) -> Tuple[bytes, bytes]:
    """
    Get fingerprint from database of from file.
    Optionally can update database
    :param img_path: filename
    :return: tuple of filename and fingerprint
    """
    db_open()

    f_i = FileInfo(img_path, from_db=True, from_fs=True)
    return f_i.file.bytes, f_i.fingerprint


def filter_by_prefix(prefix: str):
    return lambda fn: FilePath(fn).str.startswith(prefix)


def filter_by_glob(glob: str):
    return lambda fn: FilePath(fn).path.match(glob)


def filter_by_re(pattern: Union[str, Pattern[str]]):
    if not hasattr(pattern, 'match'):
        pattern = re.compile(pattern)
    return lambda fn: pattern.match(FilePath(fn).str)


def db_filter(*, prefix=None, glob=None, pattern=None):
    ret = []
    if prefix:
        ret.append(filter_by_prefix(prefix))
    if glob:
        ret.append(filter_by_glob(glob))
    if pattern:
        ret.append(filter_by_re(pattern))
    return ret


def db_all(*, filters=None, where=None, where_params=None) -> Generator[FileInfo, None, None]:
    db_open()
    if where:
        cursor = DB.execute('SELECT * FROM images WHERE ' + where, where_params)
    else:
        cursor = DB.execute('SELECT * FROM images')
    for fn, *row in cursor:
        skip = False
        if filters:
            for f in filters:
                skip = skip or (not f(fn))
        if not skip:
            yield FileInfo(fn, from_row=row)


ROT = ['ROT0', 'ROT90', 'ROT180', 'ROT270']


# noinspection PyListCreation
def find_dup(fname: FP_input,
             threshold: float = 100.0,
             filter_db: Optional[List[Callable[[bytes], bool]]] = None) \
        -> Generator[Tuple[str, bytes, str, float], None, None]:
    """
    Find possible duplicated of an image
    :param fname: filename of an image
    :param threshold: Threshold for similarity
    :param filter_db: optional filter for database records
    :return: sequence of duplicates: tuple of filename, fingerprint, rotation
    """
    if filter_db is None:
        filter_db = []
    db_open()
    f_i = FileInfo(fname, from_db=True, from_fs=True)
    for f_d in db_all(filters=filter_db, where="md5sum=?", where_params=(f_i.md5sum,)):
        if f_d.file.str == f_i.file.str:
            continue
        yield f_d.file.str, f_d.fingerprint, "MD5", 100.0

    if len(f_i.fingerprint) < 32:
        print(f"Warning: no fingerprint for {f_i.file}!", file=sys.stderr)
        return
    fpl = [f_i.fingerprint]
    fpl.append(fp_rot90(fpl[-1]))
    fpl.append(fp_rot90(fpl[-1]))
    fpl.append(fp_rot90(fpl[-1]))
    for fp0, r in zip(fpl, ROT):
        for f_d in db_all(filters=filter_db, where="fingerprint=?", where_params=(fp0,)):
            if f_d.md5sum == f_i.md5sum:
                continue
            yield f_d.file.str, f_d.fingerprint, r, 100.0

    if threshold == 100.0:
        return

    for f_d in db_all(filters=filter_db):
        if f_d.file.str == f_i.file.str:
            continue
        for fp0, r in zip(fpl, ROT):
            dif = fp_diff(fp0, f_d.fingerprint)
            if dif == 100.0:
                continue
            if dif > threshold:
                yield f_d.file.str, f_d.fingerprint, r, dif


if __name__ == '__main__':
    pass
