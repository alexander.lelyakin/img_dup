#!/usr/bin/env python3
import re
import sys
from pathlib import Path
from typing import Optional, Tuple, Callable, Generator, Union, Pattern, List

import bsddb3

from fingerprint import img_fingerprint, fp_rot90, fp_diff, fp_hex
from util import FilePath, FP_input

DB: Optional[bsddb3.db.DB] = None


def db_open(name: FP_input = '~/images.db') -> bsddb3.db.DB:
    """
    Open database
    :param name: database filename
    :return: the database
    """
    global DB
    if not DB:
        name = FilePath(name)
        DB = bsddb3.db.DB()
        DB.open(name.str, None, bsddb3.db.DB_HASH, bsddb3.db.DB_CREATE)
    return DB


def db_fingerprint(img_name: FP_input) -> bytes:
    """
    Get the fingerprint of file from database
    :param img_name: filename
    :return: fingerprint
    """
    db_open()
    return DB.get(FilePath(img_name).bytes, b'')


def db_update(iname: FP_input, fp: Optional[bytes] = None) -> bytes:
    """
    Update fingerprint in the database
    :param iname: filename
    :param fp: fingerprint. If not specified will be calculated
    :return: fingerprint
    """
    db_open()
    iname = FilePath(iname)
    if fp is None:
        fp = img_fingerprint(iname.path)
    if fp:
        DB[iname.bytes] = fp
        return fp
    else:
        if iname.bytes in DB:
            del DB[iname.bytes]
        return b''


def fingerprint(img_path: FP_input, update: bool = False) -> Tuple[bytes, bytes]:
    """
    Get fingerprint from database of from file.
    Optionally can update database
    :param img_path: filename
    :param update: can the function update database or not
    :return: tuple of filename and fingerprint
    """
    db_open()
    img_path = FilePath(img_path)
    if not img_path.exists():
        if update:
            db_update(img_path, b'')
        return img_path.bytes, b''

    fp = db_fingerprint(img_path)
    if not fp:
        if update:
            fp = db_update(img_path)
        else:
            fp = img_fingerprint(img_path)

    return img_path.bytes, fp


def filter_by_prefix(prefix: str):
    return lambda fn: FilePath(fn).str.startswith(prefix)


def filter_by_glob(glob: str):
    return lambda fn: FilePath(fn).path.match(glob)


def filter_by_re(pattern: Union[str, Pattern[str]]):
    if not hasattr(pattern, 'match'):
        pattern = re.compile(pattern)
    return lambda fn: pattern.match(FilePath(fn).str)


def db_filter(*, prefix=None, glob=None, pattern=None):
    ret = []
    if prefix:
        ret.append(filter_by_prefix(prefix))
    if glob:
        ret.append(filter_by_glob(glob))
    if pattern:
        ret.append(filter_by_re(pattern))
    return ret


ROT = ['ROT0', 'ROT90', 'ROT180', 'ROT270']


# noinspection PyListCreation
def find_dup(fname: FP_input,
             threshold: float = 90.0,
             filter_db: List[Optional[Callable[[bytes], bool]]] = None) \
        -> Generator[Tuple[str, bytes, str], None, None]:
    """
    Find possible duplicated of an image
    :param fname: filename of an image
    :param threshold: Threshold for similarity
    :param filter_db: optional filter for database records
    :return: sequence of duplicates: tuple of filename, fingerprint, rotation
    """
    if filter_db is None:
        filter_db = []
    db_open()
    fname = FilePath(fname)
    fpl = [fingerprint(fname)[1]]
    fpl.append(fp_rot90(fpl[-1]))
    fpl.append(fp_rot90(fpl[-1]))
    fpl.append(fp_rot90(fpl[-1]))

    for fn, fp in DB.items():
        if fn == fname.bytes:
            continue
        skip = False
        for f in filter_db:
            skip = skip or (not f(fn))
        if skip:
            continue

        for fp0, r in zip(fpl, ROT):
            dif = fp_diff(fp0, fp)
            if dif > threshold:
                yield fn.decode(FilePath.ENCODING), fp, r, dif


def print_fn_fp(fn: FP_input, fp: bytes):
    fn = FilePath(fn)
    print(f'{fn} {fp_hex(fp)}')


def main():
    db_open()
    if len(sys.argv) < 2:
        print("Usage: im_dup find filename", file=sys.stderr)
        print("Or:    find <find arguments> -print | im_dup update|scan", file=sys.stderr)
        print("Or:    im_dup list", file=sys.stderr)
        sys.exit(10)
    if sys.argv[1] == 'find':
        for fn, fp, rot in find_dup(sys.argv[2]):
            print(f'{rot}, {fp:}, fn')
    elif sys.argv[1] == 'update' or sys.argv[1] == 'scan':
        for line in sys.stdin:
            line = line.rstrip('\n')
            print_fn_fp(*fingerprint(line, sys.argv[1] == 'update'))
    elif sys.argv[1] == 'list':
        for fn, fp in DB.items():
            print_fn_fp(fn, fp)
    elif sys.argv[1] == 'delete':
        pattern = sys.argv[2]
        to_delete = []
        for fn, fp in DB.items():
            p = Path(fn.decode('utf-8'))
            if p.match(pattern):
                to_delete.append(fn)
        for fn in to_delete:
            del DB[fn]
    else:
        print("Unknown subcommand:", sys.argv[1], file=sys.stderr)
        sys.exit(11)


if __name__ == '__main__':
    main()
