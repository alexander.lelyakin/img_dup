# /usr/bin/python3

import sys
import argparse
from pathlib import Path
from typing import List

from fingerprint import fp_hex
from util import FilePath, proceed_args
import fp_db_sqlite
import os

parser = argparse.ArgumentParser()
parser.add_argument("--prefix", type=str, default=None)
parser.add_argument("--glob", type=str, default=None)
parser.add_argument("--pattern", type=str, default=None)
parser.add_argument("--stdin", "-c", action='store_true', default=False)
parser.add_argument('img', type=str, nargs='+')
parser.add_argument("--action", type=str, default='list',
                    choices=['remove', 'symlink', 'hardlink', 'list'])
parser.add_argument("--only-one", "-1", action="store_true", default=False)
parser.add_argument("--threshold", type=float, default=100.0)
parser.add_argument("--verbose", "-v", action="store_true", default=False)
parser.add_argument("--ext", type=str, nargs='*')

args = parser.parse_args(sys.argv[1:])
action: str = args.action
threshold: float = args.threshold
verbose: bool = args.verbose
only_one: bool = args.only_one
ext = args.ext
if ext is None:
    ext = [".jpg", ".jpeg"]
prefix: str = args.prefix
if prefix is not None:
    prefix = str(Path(prefix).expanduser().resolve())

filters = fp_db_sqlite.db_filter(prefix=prefix,
                                 glob=args.glob,
                                 pattern=args.pattern)
fp_db_sqlite.db_open()


def find_dup(path):
    duplicates = list(fp_db_sqlite.find_dup(path, threshold=threshold, filter_db=filters))
    nd = len(duplicates)
    if nd == 0:
        return
    if action == 'list':
        if verbose:
            print(path)
            for dup, fp, rot, dif in duplicates:
                print(f'-> {dif}% {rot} {dup} {fp_hex(fp)}')
    else:
        if nd > 1 and only_one:
            print(f'{path} has {nd} duplicates!.')
            for dup, fp, rot, dif in duplicates:
                print(f'-> {dif}% {rot} {dup} {fp_hex(fp)}')
            return
        dest = FilePath(duplicates[0][0])
        if action == "remove":
            if verbose:
                print(f"Removed: {path}")
            path.path.unlink()
        elif action == "symlink":
            if verbose:
                print(f"SymLinked: {path}<-{dest}")
            path.path.unlink()
            path.path.symlink_to(dest.path)
        elif action == "hardlink":
            if verbose:
                print(f"Linked: {path}<-{dest}")
            path.path.unlink()
            os.link(dest.str, path.str)
        else:
            print(f"Unknown action {action}!", file=sys.stderr)
            sys.exit(1)
        pass
    pass


images: List[str] = args.img
if args.stdin:
    images.append('-')

proceed_args(args=images, action=find_dup, extensions=ext)
