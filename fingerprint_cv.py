import gmpy2
import cv2
import numpy as np

from util import FilePath, FP_input


def _show(img: np.ndarray):
    cv2.namedWindow('Image', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Image', (256, 256))
    cv2.imshow('Image', img)
    cv2.waitKey(0)
    cv2.destroyWindow('Image')
    pass


def hist(im: np.ndarray, ch: int, *, hist_size=256) -> bytes:
    h: np.ndarray = cv2.calcHist(im, [ch], None, [hist_size], [0, 256])
    h = h.reshape(h.size)
    return np.packbits(h > np.median(h)).tobytes()


def img_fingerprint(img_name: FP_input) -> bytes:
    """
    Calculate a fingerprint of image file
    :param img_name: filename
    :return: fingerprint as 32 bytes string
    """
    try:
        img_name = FilePath(img_name)
        if not img_name.exists():
            return b''
        im: np.ndarray = cv2.imread(img_name.path)
        im = cv2.resize(im, (256, 256), cv2.INTER_LINEAR)
        h_b = hist(im, 0)
        h_g = hist(im, 1)
        h_r = hist(im, 2)

        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        im = cv2.GaussianBlur(im, 3, 1.0)
        im = cv2.resize(im, (16, 16), cv2.INTER_AREA)
        med = np.median(im)
        fp = np.packbits(im > med).tobytes()
        return fp + h_b + h_g + h_r
    except IOError:
        return b''


def fp_rot90(fp: bytes) -> bytes:
    """
    Rotate image fingerprint 90 deg
    :param fp: fingerprint
    :return: rotated fingerprint
    """
    thumb = np.unpackbits(np.frombuffer(fp[:32], dtype='uint8')).reshape((16, 16))
    thumb = np.rot90(thumb)
    return np.packbits(thumb).tobytes() + fp[32:]


def fp_show(fp: bytes) -> None:
    """
    Show fingerprint image on screen
    :param fp: fingerprint
    :return:
    """
    thumb = np.unpackbits(np.frombuffer(fp[:32], dtype='uint8')).reshape((16, 16))
    _show(thumb.astype('uint8') * 255)
    pass


def fp_diff(fp1: bytes, fp2: bytes) -> float:
    """
    Estimate difference between fingerprints in percents
    :param fp1: one fingerprint
    :param fp2: another fingerprint
    :return: difference: 100% - same; 0% - inverted, 50% unrelated
    """
    bitdiff = gmpy2.popcount(int.from_bytes(fp1, 'little') ^ int.from_bytes(fp2, 'little'))
    return (1.0 - bitdiff / (8 * len(fp1))) * 100.0


def fp_hex(fp: bytes) -> str:
    """
    Convert fingerprint to printable hex string
    :param fp: fingerprint
    :return: printable representation of fp
    """
    return ''.join('%02x' % b for b in fp)
