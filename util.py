import sys
from pathlib import Path
from typing import Union, Optional, List, Callable

FP_input = Union[str, bytes, Path, 'FilePath']


class FilePath:
    """
    Class representing a file path with
    easy conversion to various representations.
    """
    ENCODING = 'UTF-8'

    __slots__ = ['path']

    path: Path

    def __init__(self, fn: FP_input):
        """
        Normalize file name and keep it as Path object
        :param fn: input file name
        """
        if isinstance(fn, FilePath):
            self.path = fn.path
            return

        if isinstance(fn, bytes):
            fn = fn.decode(self.ENCODING)

        self.path = Path(fn).expanduser().resolve()

    @property
    def str(self) -> str:
        """
        Path as string
        :return: return path as string
        """
        return str(self.path)

    @property
    def bytes(self) -> bytes:
        """
        Path as bytes
        :return: return path as bytes
        """
        return self.str.encode(self.ENCODING)

    def exists(self) -> bool:
        """
        :return: wether file exists
        """
        return self.path.exists()

    def __str__(self):
        return self.str

    def __bytes__(self):
        return self.bytes


def proceed_path(p: Union[str, Path], *,
                 extensions: List[str],
                 action: Callable[[Path], None]):
    p = Path(p).expanduser()
    if not p.exists():
        print(f"Path {p} does not exists!", file=sys.stderr)
        return

    if p.is_symlink():
        return

    if p.is_dir():
        print(f"Entered dir: {p}")
        for f in p.iterdir():
            if (f.is_file() and (f.suffix.lower() in extensions)) or f.is_dir():
                proceed_path(f, extensions=extensions, action=action)
                pass
            pass

    elif p.is_file():
        action(p)

    pass


def proceed_args(args: Optional[List[str]] = None, *,
                 extensions: List[str],
                 action: Callable[[Path], None]) -> None:
    if args is None:
        args = sys.argv[1:]
    if len(args) == 0:
        proceed_path('.', extensions=extensions, action=action)
    else:
        for arg in args:
            if arg == '-':
                for line in sys.stdin:
                    line = line.rstrip('\n')
                    proceed_path(line, extensions=extensions, action=action)
            else:
                proceed_path(arg, extensions=extensions, action=action)
