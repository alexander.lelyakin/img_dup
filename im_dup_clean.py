#!/usr/bin/python3

import fp_db_sqlite

DB = fp_db_sqlite.db_open()
to_delete = []
for fn in fp_db_sqlite.db_all():
    fp = fn.file.path
    if fp.is_symlink() or (not fp.exists()):
        to_delete.append(fn)

for fn in to_delete:
    fn.size = 0
    fn.put_db()
